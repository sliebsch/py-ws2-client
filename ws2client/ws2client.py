import json
import urllib.parse
from datetime import datetime
from typing import Literal, Any

import requests
from requests import RequestException


class WS2ClientException(Exception):
    pass


Ordering = Literal['asc', 'desc']


class _JsonEncoder(json.JSONEncoder):

    def default(self, o: Any) -> Any:
        if isinstance(o, datetime):
            return o.isoformat()
        return super().default(o)


class WS2Client:
    def __init__(self,
                 *,
                 server: str = 'https://weather-v2.earthtv.com',
                 token: str,
                 server_timeout: float = 60):
        self._server = server
        self._server_timeout = server_timeout
        self._headers = {'Content-Type': 'application/json',
                         'Accept': 'application/json',
                         'Authorization': f'Bearer {token}'}

    def observations(self,
                     *,
                     lat: float,
                     lon: float,
                     radius: float = None,
                     stations: list[str] = None,
                     limit: int = None,
                     order: list[dict[str, Ordering]] = None,
                     providers: list[str] = None,
                     startDate: datetime = None,
                     endDate: datetime = None,
                     stationsNewestOnly: bool = None) -> dict:
        p = dict(radius=radius, stations=stations, limit=limit, order=order,
                 providers=providers, startDate=startDate, endDate=endDate,
                 stationsNewestOnly=stationsNewestOnly)
        opt_params = {k: v for k, v in p.items() if v is not None}
        return self._query('observations', dict(lat=lat, lon=lon) | opt_params)

    def forecasts(self,
                  *,
                  lat: float,
                  lon: float,
                  radius: float = None,
                  stations: list[str] = None,
                  limit: int = None,
                  order: list[dict[str, Ordering]] = None,
                  providers: list[str] = None,
                  startDate: datetime = None,
                  endDate: datetime = None,
                  forecastStartDate: datetime = None,
                  forecastEndDate: datetime = None) -> dict:
        p = dict(radius=radius, stations=stations, limit=limit, order=order,
                 providers=providers, startDate=startDate, endDate=endDate,
                 forecastStartDate=forecastStartDate, forecastEndDate=forecastEndDate)
        opt_params = {k: v for k, v in p.items() if v is not None}
        return self._query('forecasts', dict(lat=lat, lon=lon) | opt_params)

    def daily_forecasts(self,
                        *,
                        lat: float,
                        lon: float,
                        radius: float = None,
                        stations: list[str] = None,
                        limit: int = None,
                        order: list[dict[str, Ordering]] = None,
                        providers: list[str] = None,
                        startDate: datetime = None,
                        endDate: datetime = None,
                        forecastStartDate: datetime = None,
                        forecastEndDate: datetime = None) -> dict:
        p = dict(radius=radius, stations=stations, limit=limit, order=order,
                 providers=providers, startDate=startDate, endDate=endDate,
                 forecastStartDate=forecastStartDate, forecastEndDate=forecastEndDate)
        opt_params = {k: v for k, v in p.items() if v is not None}
        return self._query('dailyForecasts', dict(lat=lat, lon=lon) | opt_params)

    def _query(self, endpoint: str, params: dict) -> dict:
        url = urllib.parse.urljoin(self._server, endpoint)
        try:
            response = requests.post(url,
                                     data=json.dumps(params, cls=_JsonEncoder),
                                     headers=self._headers,
                                     timeout=self._server_timeout)
        except (RequestException, ConnectionError) as err:
            raise WS2ClientException(f"Failed to connect to WeatherServer2: {err}")

        if response.status_code == 200:
            return response.json()
        else:
            raise WS2ClientException(
                f"Failed to query weather ({endpoint}); WeatherServer2 API response: "
                f"{response.status_code} {response.reason}")
