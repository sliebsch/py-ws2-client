from setuptools import setup, find_packages

with open('ws2client/VERSION') as f:
    VERSION = f.read().strip()

setup(
    name='ws2client',
    version=VERSION,
    description='A WeatherServer2 client',
    author='Sascha Liebsch',
    author_email='info@earthtv.com',
    #license='commercial',
    #classifiers=[],
    #keywords='',
    packages=find_packages(include=['ws2client*'], exclude=['docs', 'tests']),
    install_requires=[
        'requests ~= 2.0'
    ],

    extras_require={},
    package_data={
        'ws2client': ['VERSION'],
    },
)
